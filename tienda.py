# CLASES
import csv, os, time, operator,sys

# FUNCIONES PRINCIPALES

def verificarSistema():
    if os.name == 'nt':
        return "windows"
    else:
        return "unix"

def limpiarPantalla():
    sistema = verificarSistema()
    if (sistema == "windows"):
        os.system('cls')
    else:
        os.system('clear')

def obtenerNombreBD(seccion): # FUNCION PARA OBTENER EL NOMBRE DE LA BD
    if (seccion == 'cliente'):
        return 'clientes.bd'
    elif (seccion == 'articulo'):
        return 'inventario.bd'
    elif (seccion == 'apartado'):
        return 'apartados.bd'

def escribirBD(seccion,datos): # FUNCION PARA ESCRIBIR EN LA BD
    link = open(obtenerNombreBD(seccion),'a')
    link.write(datos)
    print('Datos ingresados correctamente!')

def ordenarBD(seccion,columna): # FUNCION PARA ORDENAR POR CAMPO ASCENDENTE
    data = csv.reader(open(obtenerNombreBD(seccion),"r"))
    datasorted = sorted(data, key=operator.itemgetter(columna))
    result = open(obtenerNombreBD(seccion), "w")

    for row in datasorted:
        temp = str(row)
        temp = temp.replace("[","")
        temp = temp.replace("]","")
        temp = temp.replace(" '","")
        temp = temp.replace("'","")
        result.write(temp+"\n")

def leerBD(seccion): # FUNCION PARA LEER LA BD
    datos = open(obtenerNombreBD(seccion),'r')
    temp = datos.read()
    print(temp)

def existeBD(bd,numCampo,dato): # FUNCION PARA VERIFICAR SI EL DATO EXISTE
    datos = csv.reader(open(obtenerNombreBD(bd),"r"))
    unico = True
    for row in datos:
        if (dato == row[numCampo]):
            unico = False
    if (unico == False):
        return True
    else:
        return False

def limpiarBD(seccion): # PREPARAR BD PARA SER LEIDA
    result = open(obtenerNombreBD(seccion), "r")
    temp = str(result.read())
    temp = temp.replace("\n\n","\n")
    result = open(obtenerNombreBD(seccion), "w")
    result.write(temp)


# FUNCIONES SECUNDARIAS

def esUnico(bd,numCampo,nombre): # FUNCION PARA VERIFICAR SI EL DATO INGRESADO ES UNICO EN LA BD
    condicion = False
    while (condicion == False):
        datos = csv.reader(open(obtenerNombreBD(bd),"r"))
        unico = True
        valor = raw_input("Ingrese el "+nombre+" : \n")
        for row in datos:
            if (valor == row[numCampo]):
                unico = False
        if (unico == False):
            condicion = False
            print ("El dato no es unico!! intente de nuevo.")
        else:
            condicion = True
            return valor

def esNumero(nombre): # FUNCION PARA VERIFICAR SI ES NUMERO O NO
    condition = False
    while (condition == False):
        valor = raw_input("Ingrese el "+nombre+" : \n")
        if (valor.isdigit()):
            condition = True
            return valor
        else:
            condition = False
            print ("El dato no es numerico!! intente de nuevo.")

def obtenerLista(bd): # FUNCION PARA OBTENER LISTAS UTILES DE CLIENTES E INVENTARIO
    datos = csv.reader(open(obtenerNombreBD(bd),"r"))
    if (bd == "cliente"):
        print ("Nombre Cliente | Correo Electronico")
        for row in datos:
            print (row[1] +" | "+ row[0])
    elif (bd == "articulo"):
        print ("Codigo | Nombre Articulo | Precio")
        for row in datos:
            print (row[0] + " | "+row[1] +" | "+ row[4])
    elif (bd == "apartado"):
        print ("Codigo | Fecha | Correo del Cliente | Articulo | Saldo ")
        for row in datos:
            if (row[4] == "abierto"):
                print (row[0] + " | "+row[1] + " | "+row[2] +" | "+ row[5] +" | "+ row[7])

def obtenerAtributo(bd,numCampo,numResultado,dato): # FUNCION PARA OBTENER CAMPO EN BASE A OTRO
    datos = csv.reader(open(obtenerNombreBD(bd),"r"))
    found = False
    result = ""
    for row in datos:
        if (dato == row[numCampo]):
            result = row[numResultado]
            found = True
    
    if (found == True):
        return result
    else:
        return False

def actualizarIntenvario(articulo): # ACTUALIZA LAS EXISTENCIAS EN EL INVENTARIO LUEGO DE CADA PEDIDO
    data = csv.reader(open(obtenerNombreBD("articulo"),"r"))
    contador = 0
    objetivo = 0
    for row in data:
        if (articulo  == row[1]):
            objetivo = contador
        contador = contador + 1
        print("test")
    
    data = csv.reader(open(obtenerNombreBD("articulo"),"r"))
    modificacion = list(data)

    modificacion[objetivo][2]=str(int(modificacion[objetivo][2])-1)
    modificacion[objetivo][3]=str(int(modificacion[objetivo][3])+1)

    if (int(modificacion[objetivo][2]) < 0):
        print("No hay articulos suficientes!")
        raw_input()
        return False
    else:
        data = csv.writer(open(obtenerNombreBD("articulo"), 'w'))
        data.writerows(modificacion)
        return True

def actualizarSaldoCliente(correo,cantidad,operacion): # ACTUALIZA EL SALDO DEL CLIENTE LUEGO DE HACER UN APARTADO O UN ABONO
    data = csv.reader(open(obtenerNombreBD("cliente"),"r"))
    contador = 0
    objetivo = 0
    for row in data:
        if (correo  == row[0]):
            objetivo = contador
        contador = contador + 1

    data = csv.reader(open(obtenerNombreBD("cliente"),"r"))
    modificacion = list(data)

    if (operacion == "suma"):
        modificacion[objetivo][3]=str(int(modificacion[objetivo][3])+cantidad)
    else:
        modificacion[objetivo][3]=str(int(modificacion[objetivo][3])-cantidad)

        ## ACTUALIZAR PEDIDO

    data = csv.writer(open(obtenerNombreBD("cliente"), 'w'))
    data.writerows(modificacion)

def procesarApartado(): # PROCESA EL APARTADO Y HACE TODOS LOS CAMBIOS NECESARIOS EN LAS OTRAS TABLAS
    limpiarPantalla()
    condicion = False
    while (condicion == False):
        nApartado = esUnico("apartado",0,"numero de apartado")
        fecha = time.strftime("%Y-%m-%d")
        print("\n -- Lista de Clientes --")
        obtenerLista("cliente")
        correo = raw_input("Ingrese el correo electronico: ")
        print("\n -- Lista de Articulos --")
        obtenerLista("articulo")
        producto = raw_input("Ingrese el nombre del producto :")
        precio = obtenerAtributo("articulo",1,4,producto)
        codigo = obtenerAtributo("articulo",1,0,producto)
        saldo = precio
        correoValido = existeBD("cliente",0,correo)
        productoValido = existeBD("articulo",1,producto)
        if ((correoValido == True) and (productoValido == True)):
            condicion = True
        else:
            print("El correo o producto digitados no existen!! intente de nuevo.")
            raw_input()
            limpiarPantalla()
    datos = nApartado + "," + fecha + "," + correo + "," + codigo + ",abierto," + producto + "," + precio + "," + saldo

    if (actualizarIntenvario(producto) == True):
        limpiarBD("articulo")
        escribirBD("apartado",datos+"\n")
        actualizarSaldoCliente(correo,int(saldo),"suma")
        limpiarBD("cliente")
    else:
        print("No se ha podido completar el pedido por falta de existencias!")
        raw_input()

def menu(opcion): # FUNCION PARA CARGAR EL MENU DE LA APLICACION
    if (opcion == "1"):
        limpiarPantalla()
        mCliente = raw_input("1.Ingresar Cliente \n2.Consultar Clientes\n")
        if (mCliente == "1"):
            limpiarPantalla()
            correo = esUnico("cliente",0,"correo electronico")
            nombre = raw_input("Ingrese el nombre del cliente: \n")
            tel = esNumero("telefono")
            datos = correo + "," + nombre  + "," + tel  + ",0"
            limpiarPantalla()
            escribirBD("cliente",datos+"\n")
            ordenarBD("cliente",1)
        elif (mCliente == "2"):
            limpiarPantalla()
            leerBD("cliente")
            print("")
        else:
            return "Opcion invalida"    
    elif (opcion == "2"):
        limpiarPantalla()
        mInventario = raw_input("1.Ingresar Articulo \n2.Consultar Articulos\n")
        if (mInventario == "1"):
            limpiarPantalla()
            codigo = esUnico("articulo",0,"codigo")
            nombre = raw_input("Ingrese el nombre del articulo: \n")
            cExistencia = raw_input("Ingrese la cantidad de " +nombre+ " existentes: \n")
            cApartado = raw_input("Ingrese la cantidad de " +nombre+ " apartados: \n")
            precio =  raw_input("Ingrese el precio de " +nombre+ ": \n")
            datos = codigo + "," + nombre  + "," + cExistencia  + "," + cApartado + "," + precio
            limpiarPantalla()
            escribirBD("articulo",datos+"\n")
            ordenarBD("articulo",0)
        elif (mInventario == "2"):
            limpiarPantalla()
            leerBD("articulo")
            print("")
        else:
            return "Opcion invalida"  
    elif (opcion == "3"):
      limpiarPantalla()
      mApartado = raw_input("1.Crear Apartado \n2.Hacer Abono \n3.Ver Apartados Abiertos\n")
      if (mApartado == "1"):
        limpiarPantalla()
        procesarApartado()
      elif (mApartado == "2"):
        limpiarPantalla()
        print("test")
      elif (mApartado == "3"):
        limpiarPantalla()
        obtenerLista("apartado")
        raw_input()
    elif (opcion == "4"):
        sys.exit()
    else:
        print ("opcion no valida")

# CUERPO DEL PROGRAMA

limpiarPantalla()
opcion = raw_input("1.Cliente \n2.Inventario \n3.Apartados \n4.Salir \n")
menu(opcion)